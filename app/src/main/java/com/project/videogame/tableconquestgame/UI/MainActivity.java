package com.project.videogame.tableconquestgame.UI;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.project.videogame.tableconquestgame.R;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
    }



    public void button_NuevoPartida(View view){
        Intent NuevaPartida = new Intent(MainActivity.this, NuevaPartida.class);
        startActivity(NuevaPartida);
    }

    public void button_CargarPartida(View view){
        Intent CargarPartida = new Intent(MainActivity.this, NuevaPartida.class);
        startActivity(CargarPartida);
    }

    public void button_Sobre(View view){
        Intent Sobre = new Intent(MainActivity.this, NuevaPartida.class);
        startActivity(Sobre);
    }

}
