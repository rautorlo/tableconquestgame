package com.project.videogame.tableconquestgame.UI;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.project.videogame.tableconquestgame.R;


public class NuevaPartida extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_partida);
    }

    public void button_Empezar(View view){
        Intent juego = new Intent (NuevaPartida.this, juego.class);

        //Obtener parámetros desde los EditText
        final EditText nombrePartida = (EditText) findViewById(R.id.NombrePartida);
        final EditText maxturnos = (EditText) findViewById(R.id.TurnosMax);
        final EditText initialgold = (EditText) findViewById(R.id.OroInicial);
        final Spinner country = (Spinner) findViewById(R.id.sel_country);



        //Enviar a juego los parámetros
        juego.putExtra("nombrePartida", nombrePartida.getText().toString());
        juego.putExtra("maxturnos", maxturnos.getText().toString());
        juego.putExtra("initialgold", initialgold.getText().toString());
        juego.putExtra("human_country", country.getSelectedItem().toString());

        startActivity(juego);
    }

}
