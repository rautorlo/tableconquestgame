package com.project.videogame.tableconquestgame.UI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.project.videogame.tableconquestgame.R;
import com.project.videogame.tableconquestgame.UI.util.ui_connector;

import logica.*;

public class juego extends Activity {

    Bundle fromNuevaPartida;
    String nombrePartida = "default";
    int maxTurnos = 50;
    int initialGold = 500;
    String human_country_name;
    country human_country;

    mapa currentMap;
    turn_system TS;
    ui_connector UIC;

    boolean atacar = false;
    area origen_ataque;
    area destino_ataque;

    Button spain_but;
    Button france_but;
    Button germany_but;
    Button uk_but;
    Button italy_but;
    Button norway_but;
    Button sweden_but;
    Button finland_but;
    Button poland_but;
    Button russia_but;
    Button romania_but;
    Button greece_but;

    @Override
    protected void onPause() {
        super.onPause();
        TS.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        TS.resume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        //Recibir parámetros desde NuevaPartida
        fromNuevaPartida = getIntent().getExtras();
        if (fromNuevaPartida != null) {
            nombrePartida = fromNuevaPartida.getString("nombrePartida");
            String valueMT = fromNuevaPartida.getString("maxturnos");
            String valueIG = fromNuevaPartida.getString("initialgold");
            human_country_name = fromNuevaPartida.getString("human_country");

            maxTurnos = Integer.parseInt(valueMT);
            initialGold = Integer.parseInt(valueIG);

            System.out.println("El valor de nueva partida es: " + nombrePartida);
            System.out.println("El valor de maxturnos es: " + maxTurnos);
            System.out.println("El valor de initialgold es: " + initialGold);
            System.out.println("El valor de human_country_name es: " + human_country_name);
        }

        /*
        Inicializando el MAPA...
         */
        currentMap = new mapa(nombrePartida, initialGold, maxTurnos);
        currentMap.set1Human_AI(human_country_name);
        currentMap.getCountryByName(currentMap.getHumanCountry().getName()).setisMyTurn(true);
        human_country = currentMap.getHumanCountry();
        
        /*
        Inicializando la UI...
         */

        TextView country_display = (TextView) findViewById(R.id.country_display);
        TextView gold_display = (TextView) findViewById(R.id.gold_display);
        if (country_display != null && gold_display != null) {
            country_display.setText((CharSequence) currentMap.getHumanCountry().getName());

            String gold4display = "" + currentMap.getHumanCountry().getGold() + "g";
            gold_display.setText((CharSequence) gold4display);
        } else System.out.println("Displays de juego erróneos");


        /*
        Comienza el juego...
         */

        initializeCountryColours();
        TS = new turn_system(currentMap);
        UIC = new ui_connector(TS);
        TS.setUIC(UIC);

    }

    public void initializeCountryColours(){
        spain_but = (Button) findViewById(R.id.button_Spain);
        france_but = (Button) findViewById(R.id.button_France);
        germany_but = (Button) findViewById(R.id.button_Germany);
        uk_but = (Button) findViewById(R.id.button_UK);
        italy_but = (Button) findViewById(R.id.button_Italy);
        norway_but = (Button) findViewById(R.id.button_Norway);
        sweden_but = (Button) findViewById(R.id.button_Sweden);
        finland_but = (Button) findViewById(R.id.button_Finland);
        poland_but = (Button) findViewById(R.id.button_Poland);
        russia_but = (Button) findViewById(R.id.button_Russia);
        romania_but = (Button) findViewById(R.id.button_Romania);
        greece_but = (Button) findViewById(R.id.button_Greece);

        spain_but.setBackgroundResource(R.drawable.areacapitalyellow);
        france_but.setBackgroundResource(R.drawable.areacapitalblue);
        germany_but.setBackgroundResource(R.drawable.areacapitalgrey);
        uk_but.setBackgroundResource(R.drawable.areacapitalred);
        italy_but.setBackgroundResource(R.drawable.areacapitallightgreen);
        norway_but.setBackgroundResource(R.drawable.areacapitalgreen);
        sweden_but.setBackgroundResource(R.drawable.areacapitallightblue);
        finland_but.setBackgroundResource(R.drawable.areacapitalorange);
        russia_but.setBackgroundResource(R.drawable.areacapitaldarkgreen);
        poland_but.setBackgroundResource(R.drawable.areacapitalpurple);
        romania_but.setBackgroundResource(R.drawable.areacapitalmagenta);
        greece_but.setBackgroundResource(R.drawable.areacapitalturquoise);
    }

    public void conquest(army a, String area_name, country c){
        Button conquered_but =
                (area_name=="Spain_area")?spain_but:
                (area_name=="France_area")?france_but:
                spain_but;
        currentMap.getArea(area_name).setArmy(a);
        currentMap.getArea(area_name).setCountry(c);
        switch(c.getName()) {
            case "Spain":
                conquered_but.setBackgroundResource(R.drawable.areacapitalyellow);
                break;
            case "France":
                conquered_but.setBackgroundResource(R.drawable.areacapitalblue);
                break;
        }
    }

    public void attackDialog(final String area_n){

        origen_ataque = currentMap.getArea(area_n);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Ataque");
        alertDialogBuilder.setMessage("¿Quieres atacar?\nInfanteria: " + origen_ataque.getArmy().getNumInfantry() + "\nCaballería: " + origen_ataque.getArmy().getNumCavalry());

        //alertDialogBuilder.setMessage("¿Quieres atacar con tu armada?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity
                atacar = true;
                System.out.println("Atacar = "+atacar);
                System.out.println("Origen ataque = "+origen_ataque.getName());
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, just close
                // the dialog box and do nothing
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void battleDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Batalla");

        army armyA = origen_ataque.getArmy();
        army armyB = destino_ataque.getArmy();

        double diceA = (int)((Math.random()*6)+1);
        double diceB = (int)((Math.random()*6)+1);

        battle bat = new battle(armyA,armyB);
        bat.armyBattle(armyA,armyB,diceA/10,diceB/10);
        System.out.println("##### BATALLA #####");
        System.out.println("Result armyA: "+bat.getResultA());
        System.out.println("Result armyB: "+bat.getResultB());

        if(bat.isA_destroyed()){
            armyA.damagedArmy(95);
            armyB.damagedArmy(bat.getResultA());
            alertDialogBuilder.setMessage("Daños en armadas: \nAtacante: 95\nDefensor: "+bat.getResultA());
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            currentMap.getArea(origen_ataque.getName()).setArmy(armyA);
            currentMap.getArea(destino_ataque.getName()).setArmy(armyB);
        }
        else
        if(bat.isB_destroyed()){
            armyA.damagedArmy(bat.getResultB());
            armyB.damagedArmy(95);
            army armyC = armyA.armyDivided();
            currentMap.areaConquered(destino_ataque,origen_ataque.getCountry(),armyC);
            alertDialogBuilder.setMessage("Area tomada");
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            // army A conquista B
            System.out.println(currentMap.getArea(origen_ataque.getName()).getCountry().getName()+" conquistó "+destino_ataque.getName());

            currentMap.getArea(origen_ataque.getName()).setArmy(armyC);
            conquest(armyC,destino_ataque.getName(),origen_ataque.getCountry());

            }else{
            armyA.damagedArmy(bat.getResultB());
            armyB.damagedArmy(bat.getResultA());
            alertDialogBuilder.setMessage("Daños en armadas: \nAtacante: "+bat.getResultB()+"\nDefensor: "+bat.getResultA());
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            currentMap.getArea(origen_ataque.getName()).setArmy(armyA);
            currentMap.getArea(destino_ataque.getName()).setArmy(armyB);
        }




    }

    public void turnoEnemigo(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("ESPERA");
        alertDialogBuilder.setMessage("Turno enemigo");
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void pasarTurno(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("");
        alertDialogBuilder.setMessage("TURNO DE JUGADORES IA");
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void sameArea(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("ALERTA");
        alertDialogBuilder.setMessage("No puedes atacar al area origen del ejército");
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void doNotAttackDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("ALERTA");
        alertDialogBuilder.setMessage("¡Dispones de muy pocos efectivos para realizar un ataque!");
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void toMoveArmy(army AR){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Mover");
        alertDialogBuilder.setMessage("¿Quieres mover parte de tu armada a este area?\nInfantería: "+AR.getNumInfantry()+"\nCaballería: "+AR.getNumCavalry());
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                // Elegir número de unidades a mover
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, just close
                // the dialog box and do nothing
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void button_Spain(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getSpain_area().getCountry())) {
                System.out.println("Spain");
                attackDialog("Spain_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getSpain_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Spain")){
                    if(currentMap.getArea("Spain_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getSpain_area();
                        if (currentMap.getCountryByName("Spain").isAlliedArea(currentMap.getSpain_area())) {
                            toMoveArmy(currentMap.getSpain_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_France(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getFrance_area().getCountry())) {
                System.out.println("France");
                attackDialog("France_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getFrance_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("France")){
                    if(currentMap.getArea("France_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getFrance_area();
                        if (currentMap.getCountryByName("France").isAlliedArea(currentMap.getFrance_area())) {
                            toMoveArmy(currentMap.getFrance_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_UK(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getUK_area().getCountry())) {
                System.out.println("UK");
                attackDialog("UK_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getUK_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("UK")){
                    if(currentMap.getArea("UK_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getUK_area();
                        if (currentMap.getCountryByName("UK").isAlliedArea(currentMap.getUK_area())) {
                            toMoveArmy(currentMap.getUK_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Germany(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getGermany_area().getCountry())) {
                System.out.println("Germany");
                attackDialog("Germany_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getGermany_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Germany")){
                    if(currentMap.getArea("Germany_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getGermany_area();
                        if (currentMap.getCountryByName("Germany").isAlliedArea(currentMap.getGermany_area())) {
                            toMoveArmy(currentMap.getGermany_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Italy(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getItaly_area().getCountry())) {
                System.out.println("Italy");
                attackDialog("Italy_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getItaly_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Italy")){
                    if(currentMap.getArea("Italy_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getItaly_area();
                        if (currentMap.getCountryByName("Italy").isAlliedArea(currentMap.getItaly_area())) {
                            toMoveArmy(currentMap.getItaly_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Norway(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getNorway_area().getCountry())) {
                System.out.println("Norway");
                attackDialog("Norway_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getNorway_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Norway")){
                    if(currentMap.getArea("Norway_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getNorway_area();
                        if (currentMap.getCountryByName("Norway").isAlliedArea(currentMap.getNorway_area())) {
                            toMoveArmy(currentMap.getNorway_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Sweden(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getSweden_area().getCountry())) {
                System.out.println("Sweden");
                attackDialog("Sweden_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getSweden_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Sweden")){
                    if(currentMap.getArea("Sweden_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getSweden_area();
                        if (currentMap.getCountryByName("Sweden").isAlliedArea(currentMap.getSweden_area())) {
                            toMoveArmy(currentMap.getSweden_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Finland(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getFinland_area().getCountry())) {
                System.out.println("Finland");
                attackDialog("Finland_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getFinland_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Finland")){
                    if(currentMap.getArea("Finland_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getFinland_area();
                        if (currentMap.getCountryByName("Finland").isAlliedArea(currentMap.getFinland_area())) {
                            toMoveArmy(currentMap.getFinland_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Romania(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getRomania_area().getCountry())) {
                System.out.println("Romania");
                attackDialog("Romania_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getRomania_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Romania")){
                    if(currentMap.getArea("Romania_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getRomania_area();
                        if (currentMap.getCountryByName("Romania").isAlliedArea(currentMap.getRomania_area())) {
                            toMoveArmy(currentMap.getRomania_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Russia(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getRussia_area().getCountry())) {
                System.out.println("Russia");
                attackDialog("Russia_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getRussia_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Russia")){
                    if(currentMap.getArea("Russia_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getRussia_area();
                        if (currentMap.getCountryByName("Russia").isAlliedArea(currentMap.getRussia_area())) {
                            toMoveArmy(currentMap.getRussia_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Greece(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getGreece_area().getCountry())) {
                System.out.println("Greece");
                attackDialog("Greece_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getGreece_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Greece")){
                    if(currentMap.getArea("Greece_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getGreece_area();
                        if (currentMap.getCountryByName("Greece").isAlliedArea(currentMap.getGreece_area())) {
                            toMoveArmy(currentMap.getGreece_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_Poland(View view) {
        if(UIC.getButtons_Enabled()) {
            if(!atacar && human_country.equals(currentMap.getPoland_area().getCountry())) {
                System.out.println("Poland");
                attackDialog("Poland_area");
            }else if(atacar){
                if(origen_ataque.getName().equals(currentMap.getPoland_area().getName())){
                    sameArea();
                }
                else
                if(!origen_ataque.getName().equals("Poland")){
                    if(currentMap.getArea("Poland_area").getArmy().getNumSoldiers()<4){
                        doNotAttackDialog();
                    }
                    else {
                        destino_ataque = currentMap.getPoland_area();
                        if (currentMap.getCountryByName("Poland").isAlliedArea(currentMap.getPoland_area())) {
                            toMoveArmy(currentMap.getPoland_area().getArmy());
                        } else {
                            battleDialog();
                            atacar = false;
                        }
                    }
                }
            }

        }
        else turnoEnemigo();
    }

    public void button_PasarTurno(View view) {
        atacar = false;
        if(UIC.getButtons_Enabled()) {
            UIC.button_PasarTurno();
            pasarTurno();
        }
    }



}
