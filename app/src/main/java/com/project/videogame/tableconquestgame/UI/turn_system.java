package com.project.videogame.tableconquestgame.UI;

import com.project.videogame.tableconquestgame.UI.util.ui_connector;

import logica.mapa;

/**
 * Created by raullozano on 13/4/15.
 */
public class turn_system implements Runnable {

    private int turn = 0;
    private boolean pasarTurno = false;
    private int num_played_countries = 0;
    private mapa currentMap;
    private static turn_system INSTANCE = null;
    private Thread renderThread = null;
    private boolean running;
    private ui_connector UIC;

    public static turn_system getTurn_System(mapa map) {
        if (INSTANCE == null) createInstance(map);
        return INSTANCE;
    }

    private synchronized static void createInstance(mapa map) {
        if (INSTANCE == null) {
            INSTANCE = new turn_system(map);
        }
    }

    public turn_system(mapa map){
        this.currentMap = map;
    }

    public synchronized void setUIC(ui_connector UIC){
        this.UIC = UIC;
    }

    public synchronized void setPasarTurno(boolean PT){
        pasarTurno = PT;
    }

    /*
    Se comprueba que el jugador humano es país que le pasas por el argumento, en ese caso, lo deja jugar
    hasta que el mismo jugador pase el turno.

    En caso contrario, es decir, el turno es de un juegador AI, le damos el control a la AI (ahora mismo, solo pasa el turno)
     */
    public void country_turn(String country_name){
        if (currentMap.getCountryByName(country_name).isMyTurn()) {
            if (currentMap.getCountryByName(country_name).equals(currentMap.getHumanCountry())) {
                while (!pasarTurno) {
                    UIC.buttonsEnabled();
                }
                pasarTurno=false;
                currentMap.setTurnOFF(currentMap.getCountryByName(country_name));
                num_played_countries++;
            } else {
                    UIC.buttonsDisabled();
                try {
                    renderThread.sleep(1000);
                    System.out.println("JUGADOR AI PASA TURNO " +country_name);
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                //Como no hay AI, los jugadores de AI, pasan el turno sin hacer nada.
                currentMap.setTurnOFF(currentMap.getCountryByName(country_name));
                num_played_countries++;
            }

        }
    }

    public void run() {
        System.out.println("TURN SYSTEM RUNNING...");

            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            while (turn < currentMap.getMax_Turnos()){

                country_turn("Spain");
                country_turn("France");
                country_turn("Germany");
                country_turn("UK");
                country_turn("Norway");
                country_turn("Sweden");
                country_turn("Finland");
                country_turn("Russia");
                country_turn("Poland");
                country_turn("Romania");
                country_turn("Greece");
                country_turn("Italy");

                if (num_played_countries == 12) {
                    System.out.println("TURNO: " + turn);
                    turn++;
                    num_played_countries = 0;
                }
            }
            if(turn == currentMap.getMax_Turnos()){
                System.out.println("JUEGO FINALIZADO");
            }

    }

    public void resume() {
        running = true;
        renderThread = new Thread(this);
        renderThread.start();
    }

    public void pause() {
        running = false;
        while (true) {
            try {
                renderThread.join();
                return;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
