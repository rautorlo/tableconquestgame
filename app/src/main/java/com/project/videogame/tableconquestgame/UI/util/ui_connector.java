package com.project.videogame.tableconquestgame.UI.util;

import com.project.videogame.tableconquestgame.UI.turn_system;

import logica.mapa;

/**
 * Created by raullozano on 14/4/15.
 */
public class ui_connector {

    private static ui_connector INSTANCE = null;
    private turn_system TS;
    private boolean buttons_enabled = false;

    public static ui_connector getUI_connector(turn_system TS) {
        if (INSTANCE == null) createInstance(TS);
        return INSTANCE;
    }

    private synchronized static void createInstance(turn_system TS) {
        if (INSTANCE == null) {
            INSTANCE = new ui_connector(TS);
        }
    }

    public ui_connector(turn_system TS){
        this.TS = TS;
        buttons_enabled=true;
    }

    public boolean getButtons_Enabled(){
        return buttons_enabled;
    }

    public void button_PasarTurno(){
        TS.setPasarTurno(true);
        System.out.println("PASANDO TURNO...");
    }

    public void buttonsDisabled() {
        buttons_enabled = false;
    }
    public void buttonsEnabled() {
        buttons_enabled = true;

    }

}
