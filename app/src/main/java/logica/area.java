package logica;

/**
 * Created by Raúl Lozano on 04/04/2015.
 */
public class area {

    /*
    ID  - NAME      -> NEAR AREAS
    0   - Spain     -> France, Italy, Greece and UK
    1   - France    -> Spain, UK, Germany and Italy
    2   - UK        -> Spain, France, Germany and Norway
    3   - Germany   -> UK, France, Italy, Norway, Sweden and Poland
    4   - Italy     -> Spain, France, Germany, Romania and Greece
    5   - Norway    -> UK, Germany, Sweden and Finland
    6   - Sweden    -> Germany, Norway, Poland and Finland
    7   - Poland    -> Germany, Sweden, Finland, Russia and Romania
    8   - Greece    -> Spain, Italy, Romania and Russia
    9   - Romania   -> Italy, Poland, Greece and Russia
    10  - Finland   -> Norway, Sweden, Russia and Poland
    11  - Russia    -> Finland, Poland, Romania and Greece
    */

    String name;
    country country;
    army army;
    int fortress_health;    //Max health = 100
    String[] id_nearAreas = {"","","","","",""};

    public area(String name, country country, army army, int fortress_health, String narea0, String narea1, String narea2, String narea3, String narea4, String narea5){
        this.name               = name;
        this.country            = country;
        this.army               = army;
        this.fortress_health    = fortress_health;
        this.id_nearAreas[0]    = narea0;
        this.id_nearAreas[1]    = narea1;
        this.id_nearAreas[2]    = narea2;
        this.id_nearAreas[3]    = narea3;
        this.id_nearAreas[4]    = narea4;
        this.id_nearAreas[5]    = narea5;
    }

    //------------GET------------//
    public String getName(){
        return name;
    }
    public country getCountry(){
        return country;
    }
    public int getFortress_health() {
        return fortress_health;
    }
    public army getArmy() {
        return army;
    }
    public String[] getId_nearAreas() {
        return id_nearAreas;
    }

    //------------SET------------//
    public void setName(String name){
        this.name = name;
    }
    public void setCountry(country country){
        this.country = country;
    }
    public void setFortress_health(int fortress_health) {
        this.fortress_health = fortress_health;
    }
    public void setArmy(army army) {
        this.army = army;
    }

    //------------SUB------------//
    public void subFortress_health(int fortress_health) {
        this.fortress_health = ((this.fortress_health-fortress_health)<0)?0:this.fortress_health-fortress_health;
    }

    //------------CONQUEST------------//
    public void conquest(country country0, army armyCountry0){
        this.country            = country0;
        this.fortress_health    = 30;
        this.army               = armyCountry0;
    }

    //------------HEAL------------//
    public void heal(){
        this.fortress_health  = ((this.fortress_health+20)>100)?100:this.fortress_health+20;
    }

    //------------Utilities-----------//
    public boolean isNear(String name_area){
        for(int i=0;i<id_nearAreas.length;i++){
            if(id_nearAreas[i].equals(name_area)){
                return true;
            }
        }
        return false;
    }

    public String toString(){
        return
        "###################################\n"+
        "Name Area: "+name+"\n"+
        "Country: "+country+"\n"+
        "Fortress: "+fortress_health+"\n"+
        "###################################\n";
    }

}
