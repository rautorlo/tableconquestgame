package logica;

import java.util.ArrayList;

/**
 * Created by Raúl Lozano on 04/04/2015.
 */
public class army {

    ArrayList<infantry> listInfantry = new ArrayList<infantry>();
    ArrayList<cavalry> listCavalry = new ArrayList<cavalry>();


    public army(){
    }

    //------------SET------------//
    public void setDefaultArmy(){
        for(int i=0;i<10;i++){
            listInfantry.add(new infantry("infantry"));
        }
        for(int i=0;i<5;i++){
            listCavalry.add(new cavalry("cavalry"));
        }
    }
    public void setDefeatArmy(){
        listInfantry = new ArrayList<infantry>();
        this.addSoldier(new infantry("infantry"));
        listCavalry = new ArrayList<cavalry>();
    }

    //------------GET------------//
    public int getNumSoldiers(){
        return listCavalry.size()+listCavalry.size();
    }
    public int getNumInfantry(){
        return listInfantry.size();
    }
    public int getNumCavalry(){
        return listCavalry.size();
    }
    
    //------------ADD------------//
    public boolean addSoldier(soldier s){
        if(s.type_unit.equals("infantry")){
            listInfantry.add((infantry)s);
            return true;
        }
        else
        if(s.type_unit.equals("cavalry")){
            listCavalry.add((cavalry)s);
            return true;
        }
        else{
            System.out.println("WARNING: Try to add a unknow soldier to army");
            return false;
        }

    }
    public void addInfantry(int num_infantry){
        for(int i=0;i<num_infantry;i++) {
            this.addSoldier(new infantry("infantry"));
        }
    }
    public void addCavalry(int num_cavalry){
        for(int i=0;i<num_cavalry;i++) {
            this.addSoldier(new cavalry("cavalry"));
        }
    }

    //------------SUB------------//
    public boolean subSoldier(soldier s){
        if(s.type_unit.equals("infantry")){
            return listInfantry.remove((infantry)s);
        }
        else
        if(s.type_unit.equals("cavalry")){
            return listCavalry.remove((cavalry)s);
        }
        else{
            System.out.println("WARNING: Type unit doesn't exist in Army");
            return false;
        }
    }
    public void subInfantry(int num_infantry){
        for(int i=0;i<num_infantry;i++) {
            this.subSoldier(new infantry("infantry"));
        }
    }
    public void subCavalry(int num_cavalry){
        for(int i=0;i<num_cavalry;i++) {
            this.subSoldier(new cavalry("cavalry"));
        }
    }


    // Utilities
    public void damagedArmy(int percent){
        int toRemoveInf = (percent<=100)?(int)(listInfantry.size()*percent)/100:listInfantry.size()-1;
        int toRemoveCav = (percent<=100)?(int)(listCavalry.size()*percent)/100:listCavalry.size()-1;

        System.out.println("Dañando una armada en "+percent+"%");
        System.out.println("Inf. Antes -> Tamaño Infantería: "+getNumInfantry());
        System.out.println("Inf. Para remover: "+toRemoveInf);
        System.out.println("Cav. Antes -> Tamaño Caballería: "+getNumCavalry());
        System.out.println("Cav. Para remover: "+toRemoveCav);
        for(int i=0;i<toRemoveInf-1;i++){
            if(listInfantry.size()>0 && i<listInfantry.size()) {
                listInfantry.remove(i);
            }
        }
        for(int i=0;i<toRemoveCav-1;i++){
            if(listInfantry.size()>0 && i<listCavalry.size()) {
                listCavalry.remove(i);
            }
        }
        System.out.println("Inf. Después -> Tamaño Infantería: "+getNumInfantry());
        System.out.println("Cav. Después -> Tamaño Caballería: "+getNumCavalry());
    }

    public army newArmyFromArmy(area newArea, int num_newInfantry, int num_newCavalry){
        army new_army = new army();
        for(int i=0; i<num_newInfantry;i++){
            new_army.listInfantry.add(this.listInfantry.remove(i));
        }
        for(int i=0; i<num_newCavalry;i++){
            new_army.listCavalry.add(this.listCavalry.remove(i));
        }
        return new_army;
    }

    public army armyDivided(){

        int in_div = this.getNumInfantry()/2;
        int ca_div = this.getNumCavalry()/2;

        army newArmy = new army();

        newArmy.addInfantry(in_div);
        newArmy.addCavalry(in_div);

        return newArmy;
    }

    public String toString(){
        return
                "###################################/n"+
                        "ARMY OF"+"/n"+
                        "NUM Infantry: "+listInfantry.size()+"/n"+
                        "NUM Cavalry: "+listCavalry.size()+"/n"+
                        "###################################/n";
    }

}
