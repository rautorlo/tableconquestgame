package logica;

/**
 * Created by raullozano on 15/4/15.
 */
public class battle {

    private army armyA;
    private army armyB;

    private int resultA;
    private int resultB;

    boolean A_destroyed = false;
    boolean B_destroyed = false;

    public battle(army a, army b){
        armyA = a;
        armyB = b;
    }

    public int getResultB() {
        return resultB;
    }

    public int getResultA() {
        return resultA;
    }

    public boolean isA_destroyed() {
        return A_destroyed;
    }

    public boolean isB_destroyed() {
        return B_destroyed;
    }

    public void armyBattle(army armyA, army armyB, double diceA, double diceB){
        int armyApower;
        int armyBpower;

        int supplyH = 0;
        int supplyP = 0;

        int difference = armyA.getNumSoldiers()-armyB.getNumSoldiers();
        int largeBonus;

        System.out.println("DICE A (IN ARMY BATTLE): "+diceA);
        System.out.println("DICE B (IN ARMY BATTLE): "+diceB);


        // To compute averages (ARMY_A)
        for(int i=0; i<armyA.listInfantry.size();i++){
            supplyP = supplyP+armyA.listInfantry.get(i).getPower();
        }
        for(int i=0; i<armyA.listCavalry.size();i++){
            supplyP = supplyP+armyA.listCavalry.get(i).getPower();
        }
        armyApower  = supplyP/armyA.getNumSoldiers();

        // To compute averages (ARMY_B)
        for(int i=0; i<armyB.listInfantry.size();i++){
            supplyP = supplyP+armyB.listInfantry.get(i).getPower();
        }
        for(int i=0; i<armyB.listCavalry.size();i++){
            supplyP = supplyP+armyB.listCavalry.get(i).getPower();
        }
        armyBpower  = supplyP/armyB.getNumSoldiers();

        // % won of battle, + numbers means armyA wins, - numbers means armyB wins
        if(armyB.getNumSoldiers()*10<armyA.getNumSoldiers()){
            resultA = 100; // 100% of army B destroyed
            resultB = 1;
            B_destroyed = true;
        }
        else
        if(armyA.getNumSoldiers()*10<armyB.getNumSoldiers()){
            resultB = 100; // 100% of army A destroyed
            resultA = 1;
            A_destroyed = true;
        }
        else {
            // armyA larger than armyB
            if (difference > 0) {
                largeBonus = armyA.getNumSoldiers() / difference;
                resultA = (int)(((armyApower)*diceA)+largeBonus)*100;
                resultB = (int)((armyBpower)*diceB);
                // armyB larger than armyA
            } else if (difference < 0) {
                largeBonus = armyB.getNumSoldiers() / difference;
                resultB = (int)(((armyBpower)*diceB)+largeBonus)*100;
                resultA = (int)((armyApower)*diceA);
                // Equality
            } else {
                resultA = (int)((armyApower)*diceA);
                resultB = (int)((armyBpower)*diceB);
            }
        }


    }

}
