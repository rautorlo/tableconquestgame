package logica;

import java.util.ArrayList;

/**
 * Created by Raúl Lozano on 04/04/2015.
 */
public class country {

    private String name;
    private int gold;
    private int human_or_AI; //0-human 1-AI
    private ArrayList<area> listAreas = new ArrayList<area>();
    private boolean isMyTurn = false;
    private boolean alive = false;


    public country (String name, int gold){
        this.name           = name;
        this.gold           = gold;
        this.human_or_AI    = 0;
        this.alive          = true;

    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getGold() {
        return gold;
    }
    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getHuman_or_AI() {
        return human_or_AI;
    }
    public void setHuman_or_AI(int human_or_AI) {
        this.human_or_AI = human_or_AI;
    }

    public boolean isMyTurn() {
        return isMyTurn;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setisMyTurn(boolean isMyTurn) {
        this.isMyTurn = isMyTurn;
    }

    //------------ADD------------//
    public boolean addArea(area a){
        return listAreas.add(a);
    }
    public void addGold(int gold){
        this.gold = this.gold+gold;
    }

    //------------SUB------------//
    public boolean subArea(area a){
        return listAreas.remove(a);
    }
    public void subGold(int gold){
        this.gold = ((this.gold-gold)<0)?0:this.gold-gold;
    }

    public boolean isAlliedArea(area a){
        for(int i = 0; i<listAreas.size();i++){
            if(a.equals(listAreas.get(i))){
                return true;
            }
        }
        return false;
    }

    public String toString(){
        return "##### IN COUNTRY #####\n"+
        "Name: "+name+"\n"+
        "Gold: "+gold+"\n"+
        "Human/AI: "+human_or_AI+"\n";
    }
}
