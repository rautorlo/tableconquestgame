package logica;

import java.util.ArrayList;

/**
 * Created by Raúl Lozano on 04/04/2015.
 */
public class mapa {

    //Esta es la clase que contendrá a los jugadores, tanto humanos como IA, también se encargará de calcular los resultados de las batallas y pasar los turnos.
    //Una especie de clase de control

    String name;
    int initialGold;
    int turno;
    int max_turnos;

    ArrayList<country> listCountries = new ArrayList<country>();

    area Spain_area;
    area France_area;
    area UK_area;
    area Germany_area;
    area Italy_area;
    area Norway_area;
    area Sweden_area;
    area Finland_area;
    area Russia_area;
    area Poland_area;
    area Romania_area;
    area Greece_area;

    country Spain;
    country France;
    country UK;
    country Germany;
    country Italy;
    country Norway;
    country Sweden;
    country Finland;
    country Russia;
    country Poland;
    country Romania;
    country Greece;

    public mapa(String name, int initialGold, int max_turnos){
        this.name           = name;
        this.initialGold    = initialGold;
        this.max_turnos     = max_turnos;

        Spain   = new country("Spain",initialGold);
        France  = new country("France",initialGold);
        UK      = new country("UK",initialGold);
        Germany = new country("Germany",initialGold);
        Italy   = new country("Italy",initialGold);
        Norway  = new country("Norway",initialGold);
        Sweden  = new country("Sweden",initialGold);
        Finland = new country("Finland",initialGold);
        Russia  = new country("Russia",initialGold);
        Poland  = new country("Poland",initialGold);
        Romania = new country("Romania",initialGold);
        Greece  = new country("Greece",initialGold);
        
        //Initializing Countries
        listCountries.add(Spain);
        listCountries.add(France);
        listCountries.add(UK);
        listCountries.add(Germany);
        listCountries.add(Italy);
        listCountries.add(Norway);
        listCountries.add(Sweden);
        listCountries.add(Finland);
        listCountries.add(Russia);
        listCountries.add(Poland);
        listCountries.add(Romania);
        listCountries.add(Greece);

        //Initializing Areas
        Spain_area      = new area("Spain_area",Spain,new army(),100,"France_area","UK_area","Italy_area","Greece_area","unknow_area","unknow_area");
        France_area     = new area("France_area",France,new army(),100,"Spain_area","UK_area","Italy_area","Germany_area","unknow_area","unknow_area");
        UK_area         = new area("UK_area",UK,new army(),100,"Spain_area","France_area","Germany_area","Norway_area","unknow_area","unknow_area");
        Germany_area    = new area("Germany_area",Germany,new army(),100,"France_area","UK_area","Norway_area","Sweden_area","Poland_area","Italy_area");
        Italy_area      = new area("Italy_area",Italy,new army(),100,"Spain_area","France_area","Germany_area","Romania_area","Greece_area","unknow_area");
        Norway_area     = new area("Norway_area",Norway,new army(),100,"UK_area","Germany_area","Sweden_area","Finland_area","unknow_area","unknow_area");
        Sweden_area     = new area("Sweden_area",Sweden,new army(),100,"Germany_area","Norway_area","Finland_area","Poland_area","unknow_area","unknow_area");
        Finland_area    = new area("Finland_area",Finland,new army(),100,"Norway_area","Sweden_area","Poland_area","Russia_area","unknow_area","unknow_area");
        Russia_area     = new area("Russia_area",Russia,new army(),100,"Poland_area","Finland_area","Romania_area","Greece_area","unknow_area","unknow_area");
        Poland_area     = new area("Poland_area",Poland,new army(),100,"Germany_area","Sweden_area","Finland_area","Russia_area","Romania_area","unknow_area");
        Romania_area    = new area("Romania_area",Romania,new army(),100,"Italy_area","Poland_area","Russia_area","Greece_area","unknow_area","unknow_area");
        Greece_area     = new area("Greece_area",Greece,new army(),100,"Spain_area","Italy_area","Romania_area","Russia_area","unknow_area","unknow_area");

        //Initializing armys
        Spain_area.army.setDefaultArmy();
        France_area.army.setDefaultArmy();
        UK_area.army.setDefaultArmy();
        Germany_area.army.setDefaultArmy();
        Italy_area.army.setDefaultArmy();
        Norway_area.army.setDefaultArmy();
        Sweden_area.army.setDefaultArmy();
        Finland_area.army.setDefaultArmy();
        Russia_area.army.setDefaultArmy();
        Poland_area.army.setDefaultArmy();
        Romania_area.army.setDefaultArmy();
        Greece_area.army.setDefaultArmy();

    }

    public String getName() {
        return name;
    }

    public int getInitialGold() {
        return initialGold;
    }

    public int getTurno() {
        return turno;
    }

    public int getMax_Turnos() {
        return max_turnos;
    }

    public ArrayList<country> getListCountries() {
        return listCountries;
    }

    public area getArea(String area_name){
        if(area_name.equals("Spain_area")){
            return getSpain_area();
        }
        else
        if(area_name.equals("France_area")){
            return getFrance_area();
        }
        else
        if(area_name.equals("Germany_area")){
            return getGermany_area();
        }
        else
        if(area_name.equals("UK_area")){
            return getUK_area();
        }
        else
        if(area_name.equals("Italy_area")){
            return getItaly_area();
        }
        else
        if(area_name.equals("Norway_area")){
            return getNorway_area();
        }
        else
        if(area_name.equals("Sweden_area")){
            return getSweden_area();
        }
        else
        if(area_name.equals("Finland_area")){
            return getFinland_area();
        }
        else
        if(area_name.equals("Russia_area")){
            return getRussia_area();
        }
        else
        if(area_name.equals("Poland_area")){
            return getPoland_area();
        }
        else
        if(area_name.equals("Romania_area")){
            return getRomania_area();
        }
        if(area_name.equals("Greece_area")){
            return getRomania_area();
        }
        else return new area("Unknow area",null,null,100,"unknow_area","unknow_area","unknow_area","unknow_area","unknow_area","unknow_area");


    }

    public area getSpain_area() {
        return Spain_area;
    }

    public area getFrance_area() {
        return France_area;
    }

    public area getUK_area() {
        return UK_area;
    }

    public area getGermany_area() {
        return Germany_area;
    }

    public area getItaly_area() {
        return Italy_area;
    }

    public area getNorway_area() {
        return Norway_area;
    }

    public area getSweden_area() {
        return Sweden_area;
    }

    public area getFinland_area() {
        return Finland_area;
    }

    public area getRussia_area() {
        return Russia_area;
    }

    public area getPoland_area() {
        return Poland_area;
    }

    public area getRomania_area() {
        return Romania_area;
    }

    public area getGreece_area() {
        return Greece_area;
    }

    public void set1Human_AI(String country_name){
        for(int i = 0; i<listCountries.size();i++){
            country C = listCountries.get(i);
            System.out.println("set1Human_AI method. country_name (input): "+country_name+" C.getName(): "+C.getName());
            if(C.getName().equals(country_name)){
                System.out.println("Setting "+C.getName()+" as a human country");
                listCountries.get(i).setHuman_or_AI(0);
            } else {
                System.out.println("Setting "+C.getName()+" as a AI country");
                listCountries.get(i).setHuman_or_AI(1);
            }
        }
    }

    public country getHumanCountry(){
        for(int i = 0; i<listCountries.size();i++){
            country C = listCountries.get(i);
            if(C.getHuman_or_AI()==0){
                return C;
            }
        }
        return null;
    }

    public country getCountryByName(String name){
        for(int i = 0; i<listCountries.size();i++){
            country C = listCountries.get(i);
            if(C.getName()==name){
                return C;
            }
        }
        return null;
    }

    public void setTurnOFF (country c){
        boolean stop = false;
        for(int i = 0; i<listCountries.size() && !stop;i++){
            country C = listCountries.get(i);
            if(C.equals(c)){
                listCountries.get(i).setisMyTurn(false);
                if(i+1<listCountries.size()) {
                    listCountries.get(i + 1).setisMyTurn(true);
                }else {
                    listCountries.get(0).setisMyTurn(true);
                }
                stop = true;
            }
        }
    }

    public void areaConquered(area area, country new_c, army new_a){
        area.setCountry(new_c);
        area.setArmy(new_a);
    }

}
