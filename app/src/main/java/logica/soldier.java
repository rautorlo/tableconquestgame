package logica;

/**
 * Created by Raúl Lozano on 04/04/2015.
 */
public class soldier {

    int health;
    int power;
    int movements;
    String type_unit;

    public soldier (int health, int power, int movements, String type_unit){
        this.health         = health;
        this.power          = power;
        this.movements      = movements;
    }

    //------------GET------------//
    public int getHealth() {
        return health;
    }
    public int getPower() {
        return power;
    }
    public int getMovements() {
        return movements;
    }
    public String getType_unit() {
        return type_unit;
    }

    //------------SET------------//
    public void setHealth(int health) {
        this.health = health;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public void setMovements(int movements) {
        this.movements = movements;
    }
    public void setType_unit(String type_unit) {
        this.type_unit = type_unit;
    }

    //------------SUB------------//
    public void subHealth(int health){
        this.health = ((this.health-health)<0)?0:this.health-health;
    }

    //------------HEAL------------//
    public void heal(){
        this.health  = ((this.health+20)>100)?100:this.health+20;
    }

}
